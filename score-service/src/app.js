const express = require('express');
const cors = require('cors');
const app = express();
const appid = process.env.APPID;
const PORT = 5003;

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send(`hello from score service`)
});

app.listen(PORT, () => console.log(`score service conneted port: ${PORT}`));