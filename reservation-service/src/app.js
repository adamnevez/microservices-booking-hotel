const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 5002;

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send('hello from reservation service')
});

app.listen(PORT, () => console.log(`reservation service conneted port: ${PORT}`));