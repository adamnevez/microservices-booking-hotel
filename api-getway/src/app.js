const express = require('express');
const cors = require('cors');
const { createProxyMiddleware } = require('http-proxy-middleware');
const app = express();
const PORT = 5000;

app.use(express.json());
app.use(cors());

app.use('/authentication-service', createProxyMiddleware({
    target: 'http://127.0.0.1:5001',
    pathRewrite: {
        '^/authentication-service': ''
    }
}));

app.use('/reservation-service', createProxyMiddleware({
    target: 'http://127.0.0.1:5002',
    pathRewrite: {
        '^/reservation-service': ''
    }
}));

app.use('/score-service', createProxyMiddleware({
    target: 'http://127.0.0.1:5003',
    pathRewrite: {
        '^/score-service': ''
    }
}));

app.listen(PORT, () => {
    console.log(`API Getway Service conneted port: ${PORT}`);
});
