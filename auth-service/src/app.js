const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 5001;

app.use(express.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send('hello from authetication service')
});

app.listen(PORT, () => console.log(`authentication service conneted port: ${PORT}`));
